//
//  CustomTableViewCell.h
//  table api2
//
//  Created by Владислав on 28.11.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *textLabel;
@property (strong, nonatomic) IBOutlet UILabel *daLabel;
@property (strong, nonatomic) IBOutlet UILabel *dbLabel;

@end
