//
//  FirstTableViewController.h
//  table api2
//
//  Created by Владислав on 28.11.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTableViewCell.h"
#import "Entry.h"
#import "EntireViewController.h"

#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>

@interface FirstTableViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray *tableArray;

- (BOOL)connected;

@end
