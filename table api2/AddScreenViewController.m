//
//  AddScreenViewController.m
//  table api2
//
//  Created by Владислав on 29.11.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import "AddScreenViewController.h"
#import "AFNetworking.h"
#import "FirstTableViewController.h"
#import "ServerManager.h"

@interface AddScreenViewController ()

@end

@implementation AddScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelButtonTapped:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

- (IBAction)saveButtonTapped:(UIBarButtonItem *)sender {
    
    /*
    //save string
    NSString *saveString = field.text;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:saveString forKey:@"saveString"];
    [defaults synchronize];
    */
    
    [self addEntry];
    
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

- (IBAction)dismiss:(id)sender {
    
    [sender resignFirstResponder];
    
}

#pragma mark - API

-(void)addEntry {
    NSString* currentSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_session"];
    NSString* textOfTextView = textView.text;
    [[ServerManager sharedManager] addEntryWithSession:currentSession
                                                  body:textOfTextView
                                             onSuccess:^(id result) {
                                                 
                                             }
                                             onFailure:^(NSError *error, NSInteger statusCode) {
                                                 NSLog(@"error = %@, code = %d", [error localizedDescription], statusCode);
                                             }
     ];
}

/*
-(void)addEntry {
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setValue:@"AP30noL-wJ-w0rxz2C" forHTTPHeaderField:@"token"];
    
    NSString* urlString = @"https://bnet.i-partner.ru/testAPI/?test=1";
    
    NSString* ses = [self getObjectWithParamName:@"user_session"];
    
    //NSDictionary* parametersDictionary = @{@"a":@"add_entry", @"session":ses, @"body":@"Lorem ipsum"};
    NSDictionary* parametersDictionary = @{@"a":@"add_entry", @"session":ses, @"body":textView.text};
    
    [manager POST:urlString parameters:parametersDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError* err;
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&err];
        NSLog(@"addEntry response: %@", dict);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
    }];
}
*/

#pragma mark - NSUserDefaults

- (id)getObjectWithParamName:(NSString*)paramName
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:paramName];
}

@end
