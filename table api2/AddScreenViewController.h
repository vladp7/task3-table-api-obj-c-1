//
//  AddScreenViewController.h
//  table api2
//
//  Created by Владислав on 29.11.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddScreenViewController : UIViewController {
    __weak IBOutlet UITextView *textView;
}

- (IBAction)cancelButtonTapped:(UIBarButtonItem *)sender;
- (IBAction)saveButtonTapped:(UIBarButtonItem *)sender;

//Keyboard dismisses
- (IBAction)dismiss:(id)sender;

@end
