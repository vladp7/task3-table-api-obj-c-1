//
//  ServerManager.h
//  table api2
//
//  Created by Владислав on 02.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Entry.h"

@interface ServerManager : NSObject

+ (ServerManager*) sharedManager;

-(void)newSessionOnSuccess:(void(^)(NSString* session)) success
                 onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

-(void)getEntriesWithSession:(NSString*) session
        onSuccess:(void(^)(NSArray* tableArray)) success
        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

-(void)addEntryWithSession:(NSString*) session
           body:(NSString*) body
      onSuccess:(void(^)(id result)) success
      onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

@end
