//
//  EntireViewController.m
//  table api2
//
//  Created by Владислав on 29.11.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import "EntireViewController.h"

@interface EntireViewController ()

@end

@implementation EntireViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _fullTextTextView.text = _text;
    //[_fullTextLabel sizeToFit];
}

-(void)viewWillAppear:(BOOL)animated
{
    //[_fullTextLabel sizeToFit];
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)viewDidLayoutSubviews {
    //[_fullTextLabel sizeToFit];
    /*
    [super viewDidLayoutSubviews];
    _fullTextLabel.preferredMaxLayoutWidth = _fullTextLabel.frame.size.width;
    [self.view layoutIfNeeded];
    */
}

@end
