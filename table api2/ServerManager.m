//
//  ServerManager.m
//  table api2
//
//  Created by Владислав on 02.12.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import "ServerManager.h"
#import "AFNetworking.h"

@interface ServerManager ()

@property (strong, nonatomic) AFHTTPSessionManager* sessionManager;
@property (strong, nonatomic) NSString* urlString;

@end

@implementation ServerManager


+ (ServerManager*) sharedManager {
    
    static ServerManager* manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ServerManager alloc] init];
    });
    
    return manager;
}

- (id)init
{
    self = [super init];
    if (self) {
        
        //NSURL* url = [NSURL URLWithString:@"https://bnet.i-partner.ru/testAPI/"];
        self.urlString = @"https://bnet.i-partner.ru/testAPI/?test=1";
        
        self.sessionManager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        self.sessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [self.sessionManager.requestSerializer setValue:@"AP30noL-wJ-w0rxz2C" forHTTPHeaderField:@"token"];
        
    }
    return self;
}

-(void)newSessionOnSuccess:(void(^)(NSString* session)) success
                 onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    //AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    //manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    //[manager.requestSerializer setValue:@"AP30noL-wJ-w0rxz2C" forHTTPHeaderField:@"token"];
    
    //NSString* urlString = @"https://bnet.i-partner.ru/testAPI/?test=1";
    NSDictionary* parametersDictionary = @{@"a":@"new_session"};
    
    [self.sessionManager POST:self.urlString
                   parameters:parametersDictionary
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                            NSError* err;
                            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&err];
                            NSLog(@"newSession response (from ServerManager) : %@", dict);
                            //[self setObject:dict[@"data"][@"session"] withParamName:@"user_session"];
                            NSString* ses = dict[@"data"][@"session"];
                            //[self getEntries];

                            if (success) {
                                success(ses);
                            }
        
                      }
                      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                            NSLog(@"error: %@", error);
                            if (failure) {
                                //т.к. в NSURLSessionTask statusCode получается по другому, взял эти 2 стрчки отсюда: stackoverflow.com/questions/20566194/how-to-get-status-code-in-afnetworking
                                NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
                                NSInteger statusCode = response.statusCode;
                                
                                failure(error, statusCode);
                            }
                      }
     ];
}

-(void)getEntriesWithSession:(NSString*) session
        onSuccess:(void(^)(NSArray* tableArray)) success
        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    
    NSLog(@"we are here");
    
    //AFHTTPSessionManager *sessionManager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    //self.sessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    //[self.sessionManager.requestSerializer setValue:@"AP30noL-wJ-w0rxz2C" forHTTPHeaderField:@"token"];
    
    
    //NSString* urlString = @"https://bnet.i-partner.ru/testAPI/?test=1";
    NSDictionary* parametersDictionary = @{@"a":@"get_entries", @"session":session};
    
    NSMutableArray *tableArray = [[NSMutableArray alloc] init];
    
    [self.sessionManager POST:self.urlString
                   parameters:parametersDictionary
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                            NSError* err;
                            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&err];
                            NSLog(@"getEntries (from ServerManager) response: %@", dict);

                            NSMutableArray *tmp_array = dict[@"data"];
                            tmp_array = tmp_array[0];
                            NSMutableArray *tableArray = [[NSMutableArray alloc] init];
                            //NSLog(@"body0: %@", dict[@"data"][0][0][@"body"]);
                            //NSLog(@"count: %d", [dict[@"data"][0] count]);
                            //        NSDictionary *data = [dict objectForKey:@"data"];
                            for (NSDictionary *ent in tmp_array) {
                                NSLog(@"ent: %@", ent);
                                Entry* entry = [[Entry alloc] init];
                                entry.da = ent[@"da"];
                                entry.dm = ent[@"dm"];
                                entry.body = ent[@"body"];
                                entry.entryID = ent[@"id"];
                                
                                [tableArray addObject:entry];
                            }

                            if (success) {
                                success(tableArray);
                            }
                            //[self.tableView reloadData];
                            NSLog(@"_tableArray count: %d", [tableArray count]);
                      }
                      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                            NSLog(@"error: %@", error);
                            if (failure) {
                                //т.к. в NSURLSessionTask statusCode получается по другому, взял эти 2 стрчки отсюда: stackoverflow.com/questions/20566194/how-to-get-status-code-in-afnetworking
                                NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
                                NSInteger statusCode = response.statusCode;

                                failure(error, statusCode);
                            }
                      }
    ];
}

-(void)addEntryWithSession:(NSString*) session
           body:(NSString*) body
      onSuccess:(void(^)(id result)) success
      onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    //AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    //manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    //[manager.requestSerializer setValue:@"AP30noL-wJ-w0rxz2C" forHTTPHeaderField:@"token"];
    
    //NSString* urlString = @"https://bnet.i-partner.ru/testAPI/?test=1";
    
    //NSString* ses = [self getObjectWithParamName:@"user_session"];
    
    //NSDictionary* parametersDictionary = @{@"a":@"add_entry", @"session":ses, @"body":@"Lorem ipsum"};
    NSDictionary* parametersDictionary = @{@"a":@"add_entry", @"session":session, @"body":body};
    
    [self.sessionManager POST:self.urlString
       parameters:parametersDictionary
         progress:nil
          success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                NSError* err;
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&err];
                NSLog(@"addEntry response (from ServerManager): %@", dict);
                if (success) {
                    success(responseObject);
                }
          }
          failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"error: %@", error);
                if (failure) {
                    //т.к. в NSURLSessionTask statusCode получается по другому, взял эти 2 стрчки отсюда: stackoverflow.com/questions/20566194/how-to-get-status-code-in-afnetworking
                    NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
                    NSInteger statusCode = response.statusCode;

                    failure(error, statusCode);
                }
          }
    ];
}

@end
