//
//  Entry.m
//  table api2
//
//  Created by Владислав on 28.11.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import "Entry.h"

@implementation Entry

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.body = @"no text";
        self.da = @"no da";
        self.dm = @"no db";
        self.entryID = @"no entryID";
    }
    return self;
}

@end
