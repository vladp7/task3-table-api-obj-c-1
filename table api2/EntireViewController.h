//
//  EntireViewController.h
//  table api2
//
//  Created by Владислав on 29.11.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EntireViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *fullTextLabel;
@property (strong, nonatomic) IBOutlet UITextView *fullTextTextView;

@property (strong, nonatomic) NSString* text;

@end
