//
//  Entry.h
//  table api2
//
//  Created by Владислав on 28.11.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Entry : NSObject

@property (strong, nonatomic) NSString* body;
@property (strong, nonatomic) NSString* da;
@property (strong, nonatomic) NSString* dm;
@property (strong, nonatomic) NSString* entryID;

@end
