//
//  FirstTableViewController.m
//  table api2
//
//  Created by Владислав on 28.11.16.
//  Copyright © 2016 Vladislav Popov. All rights reserved.
//

#import "FirstTableViewController.h"
#import "AFNetworking.h"
#import "ServerManager.h"
#import "CustomTableViewCell.h"

@interface FirstTableViewController ()

@end

@implementation FirstTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //self.tableArray = [NSMutableArray array];
    
    if (![self connected]) {
        [self notConnectedAlertMessage];
    }
    
    //NSLog(@"UserSession: ", [self getObjectWithParamName:@"user_session"]);
    //[self addEntry];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    if ([self getObjectWithParamName:@"user_session"] == nil) {
        [self newSession];
    } else {
        [self getEntries];
    }
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_tableArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cell";
    
    //or UITableViewCell *cell = ...
    CustomTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    //cell.textLabel.frame = CGRectMake(20,20,50,50);
    //[cell.textLabel sizeToFit];
    
    cell.textLabel.text = [[_tableArray objectAtIndex:indexPath.row] body];
    
    //cell.daLabel.text = [[_tableArray objectAtIndex:indexPath.row] da];
    
    NSString* daLabelValue = [[_tableArray objectAtIndex:indexPath.row] da];
    cell.daLabel.text = [self transformDate:daLabelValue];
    
    if ([[_tableArray objectAtIndex:indexPath.row] da] != [[_tableArray objectAtIndex:indexPath.row] dm]) {
        NSString* dbLabelValue = [[_tableArray objectAtIndex:indexPath.row] dm];
        cell.dbLabel.text = [self transformDate:dbLabelValue];
    } else {
        cell.dbLabel.text = @"Не модифицировалась";
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    EntireViewController *vc;
        
    NSIndexPath *path = [self.tableView indexPathForSelectedRow];
    
    vc = [segue destinationViewController];
     
    if ([vc respondsToSelector:@selector(text)]) {
    
    vc.text = [[_tableArray objectAtIndex:path.row] body];
    }
}

#pragma mark - Check internet connection

- (BOOL)connected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

-(void)notConnectedAlertMessage {
    NSString *title = @"Ошибка сети";
    NSString *message = @"Проверьте интернет соединение";
    NSString *okText = @"OK";
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Transform date
-(NSString*)transformDate:(NSString*) strSince1970 {
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
    //NSTimeInterval timeInterval = [[[_tableArray objectAtIndex:indexPath.row] da] doubleValue];
    NSTimeInterval timeInterval = [strSince1970 doubleValue];
    //NSLog(@"Daaaaaaaaaaate: %@",[dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:timeInterval]]);
    
    NSString *stringDate = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:timeInterval]];
    return stringDate;
}

#pragma mark - API

-(void)newSession {
    [[ServerManager sharedManager] newSessionOnSuccess:^(NSString *session) {
        [self setObject:session withParamName:@"user_session"];
    }
                                             onFailure:^(NSError *error, NSInteger statusCode) {
                                                 NSLog(@"error = %@, code = %d", [error localizedDescription], statusCode);
                                             }
     ];
}

-(void)getEntries {
    NSString* currentSession = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_session"];
    [[ServerManager sharedManager] getEntriesWithSession:currentSession
                                               onSuccess:^(NSArray *tabAr) {
                                                   //_tableArray = [[NSMutableArray alloc] init];
                                                   self.tableArray = [NSMutableArray array];
                                                   [self.tableArray addObjectsFromArray:tabAr];
                                                   [self.tableView reloadData];
                                               }
                                               onFailure:^(NSError *error, NSInteger statusCode) {
                                                   NSLog(@"error = %@, code = %d", [error localizedDescription], statusCode);
                                               }
     ];
    //NSLog(@"Coooooount: %d",[self.tableArray count]);
}

/*
-(void)newSession {
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setValue:@"AP30noL-wJ-w0rxz2C" forHTTPHeaderField:@"token"];
    
    NSString* urlString = @"https://bnet.i-partner.ru/testAPI/?test=1";
    NSDictionary* parametersDictionary = @{@"a":@"new_session"};
    
    [manager POST:urlString parameters:parametersDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError* err;
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&err];
        NSLog(@"newSession response: %@", dict);
        [self setObject:dict[@"data"][@"session"] withParamName:@"user_session"];
        [self getEntries];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
    }];
}
*/

/*
-(void)getEntries {
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setValue:@"AP30noL-wJ-w0rxz2C" forHTTPHeaderField:@"token"];
    
    NSString* urlString = @"https://bnet.i-partner.ru/testAPI/?test=1";
    
    NSString* ses = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_session"];
    
    NSDictionary* parametersDictionary = @{@"a":@"get_entries", @"session":ses};
    
    [manager POST:urlString parameters:parametersDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError* err;
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&err];
        NSLog(@"getEntries response: %@", dict);
        
        NSMutableArray *tmp_array = dict[@"data"];
        tmp_array = tmp_array[0];
        _tableArray = [[NSMutableArray alloc] init];
        //NSLog(@"body0: %@", dict[@"data"][0][0][@"body"]);
        //NSLog(@"count: %d", [dict[@"data"][0] count]);
//        NSDictionary *data = [dict objectForKey:@"data"];
        for (NSDictionary *ent in tmp_array) {
            NSLog(@"ent: %@", ent);
            Entry* entry = [[Entry alloc] init];
            entry.da = ent[@"da"];
            entry.dm = ent[@"dm"];
            entry.body = ent[@"body"];
            entry.entryID = ent[@"id"];
            
            [_tableArray addObject:entry];
        }
        
        [self.tableView reloadData];
        //NSLog(@"_tableArray count: %d", [_tableArray count]);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
    }];
}
*/

/*
-(void)addEntry {
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setValue:@"AP30noL-wJ-w0rxz2C" forHTTPHeaderField:@"token"];
    
    NSString* urlString = @"https://bnet.i-partner.ru/testAPI/?test=1";
    
    NSString* ses = [self getObjectWithParamName:@"user_session"];
    
    NSDictionary* parametersDictionary = @{@"a":@"add_entry", @"session":ses, @"body":@"Lorem ipsum"};
    
    [manager POST:urlString parameters:parametersDictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError* err;
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&err];
        NSLog(@"addEntry response: %@", dict);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
    }];
}
*/

#pragma mark - NSUserDefaults

- (void)setObject:(id)object withParamName:(NSString*)paramName
{
    if (object == nil)
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:paramName];
    else
        [[NSUserDefaults standardUserDefaults] setObject:object forKey:paramName];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (id)getObjectWithParamName:(NSString*)paramName
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:paramName];
}

@end
